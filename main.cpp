#include <iostream>
#include <string>
#include <fstream>
#include <cmath>

using namespace std;

int main(){
//initialisieren
ifstream file;
ofstream out, coout("werteber.txt");
double zeit[5][20], auslenk[5][20], stand[5], standampli[5], mittelwert[5], mittelampli[5];
unsigned int zahler[5];
for(int i=0; i <20; i++){zeit[0][i]=0;zeit[1][i]=0;zeit[2][i]=0;auslenk[0][i]=0;auslenk[1][i]=0;auslenk[2][i]=0;}
out.open("maxampli.txt");

//halbperiode errechnen
stand[0] = 0;
stand[1] = 0;
standampli[0] = 0;
standampli[1] = 0;
mittelwert[0]=0;
mittelwert[1]=0;
mittelampli[0] = 0;
mittelampli[1] = 0;

for(int i = 0; i< 2; i++){
zahler[i] = 0;
if(i == 0){file.open("maxima45.txt");}
else {file.open("maximamin45.txt");}
while(!(file.eof())){
file >> zeit[i][zahler[i]] >> auslenk[i][zahler[i]];
auslenk[i][zahler[i]] /= 100;
zahler[i]++;
}
zahler[i] -=2;
for(int j = 0; j < zahler[i]; j++){
mittelwert[i] += (zeit[i][j+1] - zeit[i][j]);
if(i ==1){out << j << "\t" << sqrt(pow((auslenk[i][j]+auslenk[i][j+1])/2,2)) << " 0.005" << endl;}
}
mittelwert[i] /= zahler[i];

for(int j = 1; j < zahler[i]; j++){
stand[i] += pow(((zeit[i][j+1] - zeit[i][j])-mittelwert[i]),2);
}
stand[i] = sqrt(stand[i]/(zahler[i]*(zahler[i]-1)));
coout << endl << "Periode " << " " << mittelwert[i]*2 << " +- " << stand[i]*2 << "\t";
file.close();
}
out.close();
coout << endl;
out.open("yquer.txt");

//yquer ermitteln
int maxza[] = {9,11};
for(int i = 0; i <2;i++){
if(i == 0){
for(int j = 0; j < maxza[i]; j++){
mittelampli[i] += (auslenk[i][j]+auslenk[i][j+2]+2*auslenk[i][j+1])/4.;
}}
if(i==1){
for(int j = 0; j < maxza[i]; j++){
mittelampli[i] += (auslenk[i][j]+auslenk[i][j+2]+2*auslenk[i][j+1])/4.;
out << j << " " << (auslenk[i][j]+auslenk[i][j+2]+2*auslenk[i][j+1])/4. << " 0.005" << endl;
}
}
}

mittelampli[0] /= maxza[0];
mittelampli[1] /= (maxza[1]-0);
standampli[0] = 0;
standampli[1] = 0;

for(int i = 0; i <2;i++){
for(int j = 0; j < maxza[i]; j++){
standampli[i] += pow(((auslenk[i][j]+auslenk[i][j+2]+2*auslenk[i][j+1])/4.-mittelampli[i]),2);
}
}


for(int i = 0; i< 2; i++){
standampli[1] = sqrt(standampli[i]/(maxza[i]*(maxza[i]-1)));
coout << mittelampli[i] << " +- " << standampli[i] << endl;
}
out.close();

//phi berechnen
const double l = 2.59;
const double Nulllage = 1.11;
const double Nullf = 0.005;
double phi[3];
double phiF[3];
for(int i = 0; i <2; i++){
phi[i] = 0.5*atan((Nulllage-mittelampli[i])/l);
}

//Fehler von phi
for(int i= 0; i< 2; i++){
phiF[i] = sqrt(
standampli[i]*standampli[i]*
pow((l/(l*l+pow((Nulllage-mittelampli[i]),2))),2)
+
Nullf*Nullf*
pow((l/(l*l+pow((Nulllage-mittelampli[i]),2))),2)
);
coout << "phi " << phi[i] << " +- " << phiF[i] << endl;
}

//Gamma berechnen
const double r = 0.0078;
const double a = 0.023;
const double b = 0.103;
const double M = 10.045;
const double alp[]={M_PI/4.,-M_PI/4.};

double gamma[4];
for(int i =0; i<3;i++){
mittelwert[i] *= 2;
gamma[i] = (4*M_PI*M_PI*phi[i]*((((2/5.)*r*r+a*a)*pow((a*a+b*b-2*a*b*cos(alp[i])),1.5))/
(mittelwert[i]*mittelwert[i]*M*a*b*sin(alp[i]))));


if(i == 2){
gamma[i]= (gamma[i-1]+gamma[i-2])/2.;
}
}

coout << endl << "Gamma " << gamma[0] << "\t" << gamma[1] << endl << "Gammamittel " << gamma[2] << endl;

//Fehler gamma berechnen
double gammaF[3];
for(int i = 0; i < 2; i++){
stand[i]*=2;
gammaF[i] = sqrt(
//Fehler Phi
phiF[i]*phiF[i]*
pow((4*M_PI*M_PI*((((2/5.)*r*r+a*a)*pow((a*a+b*b-2*a*b*cos(alp[i])),1.5))/(mittelwert[i]*mittelwert[i]*M*a*b*sin(alp[i])))),2)
+
//Fehler T
stand[i]*stand[i]*
pow(((-8*M_PI*M_PI*phi[i]*((((2/5.)*r*r+a*a)*pow((a*a+b*b-2*a*b*cos(alp[i])),1.5))/(M*a*b*sin(alp[i]))))
*2*pow(mittelwert[i],-3)),2)
+	
//Fehler alpha
(M_PI/180.)*(M_PI/180.)*
pow((((4*M_PI*M_PI*phi[i]*(a*a+(2/5)*r*r))/(a*b*M*mittelwert[i]*mittelwert[i]))*(sqrt(a*a-2*a*b*cos(alp[i])+b*b)*
(3*a*b-(cos(alp[i])/(sin(alp[i])*sin(alp[i])))*(a*a-2*a*b*cos(alp[i])+b*b)))),2)

);
coout << "Gamma Fehler: " << gammaF[i] << endl;
}

gammaF[3] = sqrt(pow(gamma[0]-gamma[2],2)+ pow(gamma[1]-gamma[2],2));
coout << "Gammamittelfehler " << gammaF[3] << endl;

double abwei[3];


//Torsionsmoment berechnen
const double gammalit = 6.67259*pow(10,-11);

abwei[0] = (gamma[0]-gammalit)*100/gammalit;
abwei[1] = (gamma[1]-gammalit)*100/gammalit;
coout << "Abweichung " << abwei[0] << "\t" << abwei[1] << endl;
const double gammalitf = 0.00085*pow(10,-11);
const double mm = 20*pow(10,-3);
const double lf = 720*pow(10,-3);
const double lff = 10*pow(10,-3);
const double df = 20*pow(10,-6);
const double rf = df/2;
double torsio[3];
//coout << "Phi " << phi[0] << " " << phi[1] << endl;
//cout << gammalit << endl << df << endl << rf << endl << mm << endl << M << endl << a << endl << b << endl << lf << endl;
for(int i =0; i <2; i++){
torsio[i]=(
(gammalit*4*a*b*lf*mm*M*sin(alp[i]))/
(pow(a*a+b*b-2*a*b*cos(alp[i]),1.5)*M_PI*rf*rf*rf*rf*phi[i])
// (gammalit*M*mm*2*16*lf)/((a*a+b*b-2*a*b*cos(alp[i]))*df*df*df*df*M_PI*phi[i])
);
//cout << "Torsion " << torsio[i] << endl;
}

//Fehler der Torsion: phi, lf, alpha, gammalit
double torsiof[3];
for(int i = 0; i <2; i++){
torsiof[i]= (sqrt(
//gammalit
gammalitf*gammalitf*
pow((4*a*b*lf*mm*M*sin(alp[i]))/
(pow(a*a+b*b-2*a*b*cos(alp[i]),1.5)*M_PI*rf*rf*rf*rf*phi[i]),2)
+
//lf
lff*lff*
pow((
(gammalit*4*a*b*mm*M*sin(alp[i]))/
(pow(a*a+b*b-2*a*b*cos(alp[i]),1.5)*M_PI*rf*rf*rf*rf*phi[i])),2)
+
//phi
phiF[i]*phiF[i]*
pow((gammalit*(-4)*a*b*lf*mm*M*sin(alp[i]))/
(pow(a*a+b*b-2*a*b*cos(alp[i]),1.5)*M_PI*rf*rf*rf*rf*phi[i]*phi[i]),2)
+
//alpha
(M_PI/180.)*(M_PI/180.)*
pow(
((gammalit*4*a*b*lf*mm*M)/(M_PI*rf*rf*rf*rf*phi[i]))*
((2*(a*a+b*b)*cos(alp[i])+a*b*(cos(2*alp[i])-5))/
(2*pow((a*a-2*a*b*cos(alp[i])+b*b),2.5))),2)

));
coout << "Torsion: " << torsio[i] << " +- " << torsiof[i] << endl;
}



return 0;
}
